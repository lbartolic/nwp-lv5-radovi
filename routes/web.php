<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::group(['middleware' => 'auth'], function() {
	/*Route::get('home', [
		'use' => 'HomeController@getHome', 'as' => 'home'
	]);*/
	Route::group(['prefix' => 'dashboard', 'namespace' => 'Dashboard', 'as' => 'dashboard.'], function() {
		Route::get('home', [
			'uses' => 'DashboardController@getHome', 'as' => 'home'
		]);
		Route::post('change-locale', [
			'uses' => 'UserController@postChangeLocale', 'as' => 'user.change-locale'
		]);

		Route::group(['middleware' => 'access.role:admin'], function() {
			Route::resource('users', 'UserController');
		});
		Route::group(['middleware' => 'access.role:student'], function() {
			Route::get('tasks/student', [
				'uses' => 'TaskController@getStudentIndex', 'as' => 'tasks.get-student-index'
			]);
			Route::get('tasks/student/accepted', [
				'uses' => 'TaskController@getStudentAccepted', 'as' => 'tasks.get-student-accepted'
			]);
			Route::get('tasks/student/show/{id}', [
				'uses' => 'TaskController@show', 'as' => 'tasks.show'
			]);
			Route::post('tasks/{id}/apply', [
				'uses' => 'TaskController@postApply', 'as' => 'tasks.apply'
			]);
			Route::post('tasks/{id}/cancel-apply', [
				'uses' => 'TaskController@postCancelApply', 'as' => 'tasks.cancel-apply'
			]);
		});
		Route::group(['middleware' => 'access.role:teacher'], function() {
			Route::get('tasks/applications', [
				'uses' => 'TaskController@getApplications', 'as' => 'tasks.applications'
			]);
			Route::get('tasks/accepted', [
				'uses' => 'TaskController@getAcceptedApplications', 'as' => 'tasks.accepted-applications'
			]);
			Route::post('tasks/{id}/accept', [
				'uses' => 'TaskController@postAccept', 'as' => 'tasks.accept'
			]);
			Route::post('tasks/{id}/cancel-accept', [
				'uses' => 'TaskController@postCancelAccept', 'as' => 'tasks.cancel-accept'
			]);
			Route::resource('tasks', 'TaskController', ['except' => [
			    'show'
			]]);
		});
	});
});

Auth::routes();