<?php

return [
	'study_types' => [
		0 => 'undergraduate study',
		1 => 'graduate study',
		2 => 'field study'
	]
];