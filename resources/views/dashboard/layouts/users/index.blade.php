@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Users</div>

                <div class="panel-body">
                	<table class="table table-hover">
                		<thead>
                			<tr>
                				<th>Name</th>
                				<th>Email</th>
                				<th>Roles</th>
                				<th>Actions</th>
                			</tr>
                		</thead>
                		<tbody>
                			@foreach($users as $user)
		                    	<tr>
	                				<td>{{ $user->name }}</td>
	                				<td>{{ $user->email }}</td>
	                				<td>
	                					@foreach($user->roles as $role)
	                						<span class="label label-info">{{ $role->name }}</span>
	                					@endforeach
	                				</td>
	                				<td><a href="{{ route('dashboard.users.edit', $user->id) }}" class="btn btn-xs btn-primary">Edit</a></td>
	                			</tr>
		                    @endforeach
                		</tbody>
                	</table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
