@if (count($errors) > 0)
	<div class="alert alert-danger" role="alert">
	    <ul>
	        @foreach ($errors->all() as $error)
	            <li>{{ $error }}</li>
	        @endforeach
	    </ul>
	</div>
@endif
<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			<label>Name</label>
			{!! Form::text('name', null, ['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
			<label>Email</label>
			{!! Form::text('email', null, ['class' => 'form-control']) !!}
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group">
			<label>Roles</label>
			{!! Form::select('roles[]', $roles, (isset($selectedRoles)) ? $selectedRoles : null, ['multiple' => true, 'class' => 'form-control']) !!}
		</div>
		{!! Form::submit('Save', ['class' => 'btn btn-primary pull-right']) !!}
	</div>
</div>