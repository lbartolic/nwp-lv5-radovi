@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <ul class="list-group">
                        @if(auth()->user()->hasRole('admin'))
                            <a href="{{ route('dashboard.users.index') }}" class="list-group-item">Users & Roles</a>
                        @elseif(auth()->user()->hasRole('teacher'))
                            <a href="{{ route('dashboard.tasks.index') }}" class="list-group-item">Tasks</a>
                            <a href="{{ route('dashboard.tasks.applications') }}" class="list-group-item">Applications</a>
                            <a href="{{ route('dashboard.tasks.accepted-applications') }}" class="list-group-item">Accepted Applications</a>
                        @elseif(auth()->user()->hasRole('student'))
                            <a href="{{ route('dashboard.tasks.get-student-index') }}" class="list-group-item">Available Tasks</a>
                            <a href="{{ route('dashboard.tasks.get-student-accepted') }}" class="list-group-item">Accepted Tasks</a>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
