@if (count($errors) > 0)
	<div class="alert alert-danger" role="alert">
	    <ul>
	        @foreach ($errors->all() as $error)
	            <li>{{ $error }}</li>
	        @endforeach
	    </ul>
	</div>
@endif
<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			<label>Title (HR)</label>
			{!! Form::text('title', null, ['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
			<label>Title (EN)</label>
			{!! Form::text('title_en', null, ['class' => 'form-control']) !!}
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group">
			<label>Study type</label>
			{!! Form::select('study_type', $studyTypes, (isset($selectedStudyType)) ? $selectedStudyType : null, ['class' => 'form-control']) !!}
		</div>
	</div>
	<div class="col-sm-12">
		<div class="form-group">
			<label>Task (HR)</label>
			{!! Form::textarea('task', null, ['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
			<label>Task (EN)</label>
			{!! Form::textarea('task_en', null, ['class' => 'form-control']) !!}
		</div>
		{!! Form::submit('Save', ['class' => 'btn btn-primary pull-right']) !!}
	</div>
</div>