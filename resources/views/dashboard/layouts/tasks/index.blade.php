@extends('layouts.app')

@section('content')

@if(auth()->user()->hasRole('teacher'))
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">My Tasks</div>

                    <div class="panel-body">
                    	<table class="table table-hover">
                    		<thead>
                    			<tr>
                    				<th>Title</th>
                    				<th>Task assignment</th>
                    				<th>Study type</th>
                                    <th>Accepted</th>
                                    <th>Applicants</th>
                    				<th>Actions</th>
                    			</tr>
                    		</thead>
                    		<tbody>
                    			@foreach($tasks as $task)
    		                    	<tr>
    	                				<td>{{ $task->title }}</td>
    	                				<td>{{ str_limit($task->task, 50, "...") }}</td>
    	                				<td><span class="label label-info">{{ $task->study_type }}</span></td>
                                        <td>{{ ($task->student == null) ? 'NO' : 'YES' }}</td>
                                        <td><span class="label label-primary">{{ $task->applicants->count() }}</span></td>
    	                				<td><a href="{{ route('dashboard.tasks.edit', $task->id) }}" class="btn btn-xs btn-primary">Edit</a></td>
    	                			</tr>
    		                    @endforeach
                    		</tbody>
                    	</table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
@if(auth()->user()->hasRole('student'))
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Available Tasks</div>

                    <div class="panel-body">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Task assignment</th>
                                    <th>Study type</th>
                                    <th>Teacher</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($tasks as $task)
                                    <tr>
                                        <td>{{ $task->title }}</td>
                                        <td>{{ str_limit($task->task, 50, "...") }}</td>
                                        <td><span class="label label-info">{{ $task->study_type }}</span></td>
                                        <td><span class="label label-default">{{ $task->user->name }}</span></td>
                                        <td>
                                            @if(count($userTasks->where('id', $task->id)) > 0)
                                                {!! Form::open(['route' => ['dashboard.tasks.cancel-apply', $task->id]]) !!}
                                                    {!! Form::submit('Cancel', ['class' => 'btn btn-xs btn-danger']) !!}
                                                {!! Form::close() !!}
                                            @else
                                                {!! Form::open(['route' => ['dashboard.tasks.apply', $task->id]]) !!}
                                                    {!! Form::submit('Apply', ['class' => 'btn btn-xs btn-primary']) !!}
                                                {!! Form::close() !!}
                                            @endif
                                            <a href="{{ route('dashboard.tasks.show', $task->id) }}" class="btn btn-xs btn-info">View</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

@endsection
