@extends('layouts.app')

@section('content')

@if(auth()->user()->hasRole('teacher'))
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Applications</div>

                    <div class="panel-body">
                    	<table class="table table-hover">
                    		<thead>
                    			<tr>
                    				<th>Title</th>
                    				<th>Task assignment</th>
                    				<th>Study type</th>
                                    <th>Applicants</th>
                    				<th>Actions</th>
                    			</tr>
                    		</thead>
                    		<tbody>
                    			@foreach($tasks as $task)
    		                    	<tr>
    	                				<td>{{ $task->title }}</td>
    	                				<td>{{ str_limit($task->task, 50, "...") }}</td>
    	                				<td><span class="label label-info">{{ $task->study_type }}</span></td>
                                        <td>
                                            @foreach($task->applicants as $applicant)
                                                {{ $applicant->name }}<br>
                                            @endforeach
                                        </td>
    	                				<td>
                                            {!! Form::open(['route' => ['dashboard.tasks.accept', $task->id]]) !!}
                                                {!! Form::select('student', $task->applicants->pluck('name', 'id'), null, ['class' => 'form-control']) !!}
                                                {!! Form::submit('Accept', ['class' => 'btn btn-xs btn-primary']) !!}
                                            {!! Form::close() !!}
                                        </td>
    	                			</tr>
    		                    @endforeach
                    		</tbody>
                    	</table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

@endsection
