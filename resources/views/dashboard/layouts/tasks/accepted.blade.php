@extends('layouts.app')

@section('content')

@if(auth()->user()->hasRole('teacher'))
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Applications</div>

                    <div class="panel-body">
                    	<table class="table table-hover">
                    		<thead>
                    			<tr>
                    				<th>Title</th>
                    				<th>Task assignment</th>
                    				<th>Study type</th>
                                    <th>Student</th>
                    				<th>Actions</th>
                    			</tr>
                    		</thead>
                    		<tbody>
                    			@foreach($tasks as $task)
    		                    	<tr>
    	                				<td>{{ $task->title }}</td>
    	                				<td>{{ str_limit($task->task, 50, "...") }}</td>
    	                				<td><span class="label label-info">{{ $task->study_type }}</span></td>
                                        <td>{{ $task->student->name }}</td>
    	                				<td>
                                            <a href="{{ route('dashboard.tasks.edit', $task->id) }}" class="btn btn-xs btn-primary">Edit</a>
                                            {!! Form::open(['route' => ['dashboard.tasks.cancel-accept', $task->id]]) !!}
                                                {!! Form::submit('Cancel', ['class' => 'btn btn-xs btn-danger']) !!}
                                            {!! Form::close() !!}
                                        </td>
    	                			</tr>
    		                    @endforeach
                    		</tbody>
                    	</table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
@if(auth()->user()->hasRole('student'))
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Accepted Tasks</div>

                    <div class="panel-body">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Task assignment</th>
                                    <th>Study type</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($tasks as $task)
                                    <tr>
                                        <td>{{ $task->title }}</td>
                                        <td>{{ str_limit($task->task, 50, "...") }}</td>
                                        <td><span class="label label-info">{{ $task->study_type }}</span></td>
                                        <td>
                                            <a href="{{ route('dashboard.tasks.show', $task->id) }}" class="btn btn-xs btn-info">View</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

@endsection
