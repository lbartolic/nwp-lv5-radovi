@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Task</div>

                <div class="panel-body">
                	<div class="row">
                        <div class="col-sm-6">
                            <div class="well">
                                <h3 style="margin: 0 0 10px 0;">Title</h3>
                                {{ $task->title }}
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="well">
                                <h3 style="margin: 0 0 10px 0;">Assignment</h3>
                                {{ $task->task }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
