<?php

namespace App\Models\Role;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function roles() {
        return $this->belongsToMany(App\Models\Role\Role::class, 'user_role');
    }
}
