<?php

namespace App\Models\Task;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
	protected $fillable = [
        'title', 'task', 'study_type'
    ];

    public function user() {
    	return $this->belongsTo(\App\Models\User\User::class);
    }

    public function student() {
    	return $this->belongsTo(\App\Models\User\User::class, 'student_id');
    }

    public function applicants() {
    	return $this->belongsToMany(\App\Models\User\User::class, 'student_task');
    }

    public function getStudyTypeAttribute($value) {
    	$studyType = array_first(config('constants.study_types'), function($v, $k) use($value) {
    		return $k == $value;
    	});
    	return $studyType;
    }

    public function getTitleAttribute($value) {
    	if (auth()->user()->locale == 'en') {
    		return $this->title_en;
    	}
    	return $value;
    }

    public function getTaskAttribute($value) {
    	if (auth()->user()->locale == 'en') {
    		return $this->task_en;
    	}
    	return $value;
    }

    public function scopeAvailable($query) {
    	return $query->whereNull('student_id');
    }

    public function scopeAcceptedForUser($query, $userId) {
    	return $query->where('student_id', $userId);
    }
}
