<?php

namespace App\Models\User;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $with = ['roles'];

    public function roles() {
        return $this->belongsToMany(\App\Models\Role\Role::class, 'user_role');
    }

    public function tasks() {
        return $this->hasMany(\App\Models\Task\Task::class);
    }

    public function applications() {
        return $this->belongsToMany(\App\Models\Task\Task::class, 'student_task');
    }

    public function hasRole($role) {
        return (count($this->roles->filter(function($item) use($role) { 
            return $item->key == $role;
        })) > 0) ? true : false;
    }
}
