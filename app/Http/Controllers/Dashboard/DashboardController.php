<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function getHome() {
    	\App::setLocale(auth()->user()->locale);
    	return view('dashboard.layouts.dashboard');
    }
}
