<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Task\Task;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = auth()->user()->tasks()->with('student', 'applicants')->get();

        return view('dashboard.layouts.tasks.index', [
            'tasks' => $tasks
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $studyTypes = config('constants.study_types');
        return view('dashboard.layouts.tasks.create', [
            'studyTypes' => $studyTypes
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        auth()->user()->tasks()->create($request->all());
        return redirect()->route('dashboard.tasks.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task = Task::find($id);
        return view('dashboard.layouts.tasks.show', [
            'task' => $task
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $studyTypes = config('constants.study_types');
        $task = Task::find($id);
        return view('dashboard.layouts.tasks.edit', [
            'task' => $task,
            'studyTypes' => $studyTypes
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        auth()->user()->tasks()->find($id)->update($request->all());
        return redirect()->route('dashboard.tasks.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getStudentIndex() {
        $tasks = Task::available()->with('applicants')->get();
        $userTasks = auth()->user()->applications()->get();

        return view('dashboard.layouts.tasks.index', [
            'tasks' => $tasks,
            'userTasks' => $userTasks
        ]);
    }

    public function getStudentAccepted() {
        $tasks = Task::acceptedForUser(auth()->user()->id)->get();

        return view('dashboard.layouts.tasks.accepted', [
            'tasks' => $tasks
        ]);
    }

    public function getApplications() {
        $applications = auth()->user()->tasks()->whereNull('student_id')->with('applicants')->has('applicants')->get();

        return view('dashboard.layouts.tasks.applications', [
            'tasks' => $applications
        ]);
    }

    public function getAcceptedApplications() {
        $applications = auth()->user()->tasks()->with('applicants', 'student')->whereNotNull('student_id')->get();

        return view('dashboard.layouts.tasks.accepted', [
            'tasks' => $applications
        ]);
    }

    public function postApply(Request $request, $taskId) {
        $user = auth()->user();
        $user->applications()->attach($taskId);
        return redirect()->back();
    }

    public function postCancelApply(Request $request, $taskId) {
        $user = auth()->user();
        $user->applications()->detach($taskId);
        return redirect()->back();
    }

    public function postAccept(Request $request, $taskId) {
        $task = Task::with('applicants')->find($taskId);
        $applicantId = $request->input('student');
        $task->student_id = $applicantId;
        $task->save();
        return redirect()->back();
    }

    public function postCancelAccept(Request $request, $taskId) {
        $task = Task::find($taskId);
        $task->student_id = null;
        $task->save();
        return redirect()->back();
    }
}
